import { configuration, PossibleTerms, Term } from "./configuration"
import { Currency, money, Money, roundMoneyDown } from "./utils/money"
import { hasCcFactor, Tenant } from "./utils/tenant"
import BigNumber from "bignumber.js"
import { LoanPurpose } from "./utils/loanPurpose"
import { IndustryCodes } from "./factors"
import { TlcMccCodes, TLC_FACTORS } from "./factors/data/tlcFactors"

// formula: desiredAmount / (1 + (fee * term)
export const calculatePayoutAmount = (
    tenantCode: Tenant,
    totalAmount: Money,
    term: Term,
    loanPurpose?: LoanPurpose,
): Money => {
    const feeForTerm = getFeePerTerm(tenantCode, term, loanPurpose)
    const payoutAmount = totalAmount.value.dividedBy(feeForTerm)

    return money(payoutAmount, Currency.EUR)
}

// formula: totalAmount = payoutAmount * (1 + (fee * term)
export const recalculateTotalAmount = (
    tenantCode: Tenant,
    payoutAmount: Money,
    term: Term,
    loanPurpose?: LoanPurpose,
): Money => {
    const feeForTerm = getFeePerTerm(tenantCode, term, loanPurpose)
    const totalAmount = payoutAmount.value.multipliedBy(feeForTerm)

    return money(totalAmount, Currency.EUR)
}

interface TermsWithPayoutAmountParameters {
    tenantCode: Tenant
    revenue: Money
    payoutAmount: Money
    mccCode?: IndustryCodes
    loanPurpose?: LoanPurpose
}

export const calculateTermsWithPayoutAmount = (
    termsWithPayoutAmountParameters: TermsWithPayoutAmountParameters,
): PossibleTerms => {
    const { tenantCode, revenue, payoutAmount, mccCode, loanPurpose } =
        termsWithPayoutAmountParameters
    const { possibleTerms, maxLoanAmount: maxPayoutAmount } = configuration[tenantCode]

    const factor = getFactor(tenantCode, mccCode)

    return possibleTerms.filter((term) => {
        const maxPayoutForTerm = revenue.value.multipliedBy(factor).multipliedBy(term)

        const totalAmount = recalculateTotalAmount(tenantCode, payoutAmount, term, loanPurpose)

        const rate = calculateRate({
            totalAmount,
            term,
            revenue,
            mccCode,
            tenantCode,
        })

        const isRateValid = rate.isLessThanOrEqualTo(factor)
        const lessThanOrEqualToMaxForTerm = payoutAmount.value.isLessThanOrEqualTo(maxPayoutForTerm)
        const lessThanOrEqualToMaxLoanAmount = payoutAmount.value.isLessThanOrEqualTo(
            maxPayoutAmount.value,
        )

        return lessThanOrEqualToMaxForTerm && lessThanOrEqualToMaxLoanAmount && isRateValid
    })
}

interface RateParameters {
    totalAmount: Money
    term: number
    revenue: Money
    tenantCode: Tenant
    mccCode?: IndustryCodes
}

// formula: ((payout amount + fee) / duration) / (revenue * credit card factor)
export const calculateRate = (rateParameters: RateParameters): BigNumber => {
    const { totalAmount, term, revenue, tenantCode, mccCode } = rateParameters

    const monthlyRepayment = totalAmount.value.div(term)
    let ccFactor

    if (hasCcFactor(tenantCode)) {
        if (!mccCode)
            throw new Error(
                `Tenant ${tenantCode} requires MCC code, yet submitted mcc data is invalid`,
            )

        ccFactor = getCCFactor(tenantCode, mccCode)
    } else {
        ccFactor = new BigNumber(1)
    }

    const revenueWithFactor = revenue.value.multipliedBy(ccFactor)
    return monthlyRepayment.div(revenueWithFactor)
}

export const calculateFee = (
    tenantCode: Tenant,
    totalAmount: Money,
    term: Term,
    loanPurpose?: LoanPurpose,
): Money => {
    const payoutAmount = calculatePayoutAmount(tenantCode, totalAmount, term, loanPurpose)
    const fee = totalAmount.value.minus(payoutAmount.value)

    return money(fee, Currency.EUR)
}

type Offer = {
    maxAmount: Money
    term: number
}

const updateBest = (best: Offer, maxForTerm: BigNumber, term: number): Offer => {
    const greaterAmount = maxForTerm.isGreaterThan(best.maxAmount.value)
    const equalButShorterTerm = maxForTerm.eq(best.maxAmount.value) && term < best?.term

    if (greaterAmount || equalButShorterTerm) {
        best = {
            maxAmount: { ...money(maxForTerm, Currency.EUR) },
            term,
        }
    }

    return best
}

export const getFactor = (tenantCode: Tenant, mccCode?: IndustryCodes): BigNumber => {
    const { factors, defaultFactor } = configuration[tenantCode]

    if (!factors && !defaultFactor)
        throw new Error(`Either a defaultFactor or a list of factors must be defined`)

    if (!factors || !mccCode) return defaultFactor

    return factors(mccCode)
}

export const getCCFactor = (tenantCode: Tenant, mccCode: TlcMccCodes): BigNumber => {
    const { factors } = configuration[tenantCode]

    if (!factors)
        throw new Error(
            `Calculations are trying to incorporate the credit card factor, but MCC Factor data is undefined for ${tenantCode}`,
        )
    if (!Number(mccCode))
        throw new Error(
            `Expected industry code to be a numeric string, instead got the ${typeof mccCode}: ${mccCode}`,
        )

    return new BigNumber(TLC_FACTORS[mccCode!]?.CCFactor || TLC_FACTORS.default.CCFactor)
}

type CalculateMaxOfferWithMinTerm = {
    tenantCode: Tenant
    revenue: Money
    mccCode?: IndustryCodes
    loanPurpose?: LoanPurpose
}
export const calculateMaxOfferWithMinTerm = (params: CalculateMaxOfferWithMinTerm): Offer => {
    const { tenantCode, revenue, mccCode, loanPurpose } = params
    const { possibleTerms, maxLoanAmount } = configuration[tenantCode]

    const factor = getFactor(tenantCode, mccCode)

    let best: Offer = {
        maxAmount: { ...money("0", Currency.EUR) },
        term: 0,
    }

    for (const term of possibleTerms) {
        let maxPossiblePayout = revenue.value.multipliedBy(term).multipliedBy(factor)

        const maxTotal = recalculateTotalAmount(
            tenantCode,
            money(maxPossiblePayout, Currency.EUR),
            term,
            loanPurpose,
        )

        const isMaxLoanBelowFactorBoundary = calculateRate({
            totalAmount: maxTotal,
            term,
            revenue,
            mccCode,
            tenantCode,
        }).isGreaterThanOrEqualTo(factor)

        if (isMaxLoanBelowFactorBoundary) {
            const newPayout = calculatePayoutAmount(
                tenantCode,
                money(maxPossiblePayout, Currency.EUR),
                term,
                loanPurpose,
            )

            maxPossiblePayout = roundPayout(newPayout.value)
        }

        const maxForTerm = maxPossiblePayout.isGreaterThanOrEqualTo(maxLoanAmount.value)
            ? maxLoanAmount.value
            : maxPossiblePayout

        best = updateBest(best, maxForTerm, term)
    }

    return best
}

const roundPayout = (v: BigNumber, currency = Currency.EUR) =>
    roundMoneyDown({ value: v.div(new BigNumber(100)), currency }).value.multipliedBy(
        new BigNumber(100),
    )

const getFeePerTerm = (tenantCode: Tenant, term: Term, loanPurpose?: LoanPurpose): BigNumber => {
    const { feePerMonth } = configuration[tenantCode]

    if (!loanPurpose) {
        if (!(term in feePerMonth)) {
            if (feePerMonth instanceof BigNumber) {
                return feePerMonth.multipliedBy(term).plus(1)
            }
            throw new Error(`Fee per term must be a BigNumber for this tenant`)
        } else {
            return feePerMonth[term].plus(1)
        }
    }
    if (loanPurpose in feePerMonth && term in feePerMonth[loanPurpose]) {
        return feePerMonth[loanPurpose][term].plus(1)
    }

    throw new Error(`No fee for ${loanPurpose} and term: ${term} for tenant: ${tenantCode}`)
}

export { LoanPurpose }
