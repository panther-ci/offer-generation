export const PAY_FACTORS = {
    GASTRONOMY: "0.20",
    HAIR_AND_BEAUTY_SALON: "0.25",
    HOTEL_AND_LODGING: "0.15",
    RETAIL_AND_COMMERCE: "0.20",
    OTHER: "0.15",
} as const

export type PayFactors = keyof typeof PAY_FACTORS
