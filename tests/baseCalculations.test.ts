import {
    calculateFee,
    calculateMaxOfferWithMinTerm,
    calculatePayoutAmount,
    calculateRate,
    calculateTermsWithPayoutAmount,
    getFactor,
    recalculateTotalAmount,
    getCCFactor,
    Tenant,
    Currency,
    money,
    moneyDown,
    moneyUp,
    roundMoneyDown,
    IndustryCodes,
} from "../src"
import BigNumber from "bignumber.js"

describe("calculateFee", () => {
    test("should calculate fee for banxy", () => {
        const desiredAmount = money("1500000", Currency.EUR)
        const fee = calculateFee(Tenant.TEST, desiredAmount, 6)

        const expectedFee = money("84905.66037735849056603774", Currency.EUR)
        expect(fee).toEqual(expectedFee)
    })
})

describe("calculateRate", () => {
    test("should calculate a fixed rate as percentage of revenue for TEST", () => {
        const term = 12
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({ totalAmount, term, revenue, tenantCode: Tenant.TEST })

        // revenue * rate * term = totalAmount
        // 32_250_00 * 0.12919896640826873385 * 12 = 50_000_00
        expect(rate).toEqual(new BigNumber("0.12919896640826873385"))
    })

    test("should calculate a fixed rate as percentage of revenue for GETRAOUL", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({ totalAmount, term: 12, revenue, tenantCode: Tenant.GRL })

        // revenue * rate * term = totalAmount
        // 32_250_00 * 0.12919896640826873385 * 12 = 50_000_00
        expect(rate).toEqual(new BigNumber("0.12919896640826873385"))
    })

    test("should calculate a fixed rate as percentage of revenue for PAYONE", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({ totalAmount, term: 12, revenue, tenantCode: Tenant.PAY })

        // revenue * rate * term = totalAmount
        // 32_250_00 * 0.12919896640826873385 * 12 = 50_000_00
        expect(rate).toEqual(new BigNumber("0.12919896640826873385"))
    })

    test("should calculate a dynamic rate for BANXY", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({ totalAmount, term: 12, revenue, tenantCode: Tenant.BXY })

        expect(rate).toEqual(new BigNumber("0.12919896640826873385"))
    })

    test("should calculate a dynamic rate with 'default' factor for TST_FACTORS", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({
            totalAmount,
            term: 12,
            revenue,
            tenantCode: Tenant.TST_FACTORS,
            // @ts-expect-error Type 'string' is not assignable to type 'undefined'
            mccCode: "9999",
        })

        expect(rate).toEqual(new BigNumber("0.0117453605825698849"))
    })

    test("should calculate a dynamic rate with given factor for TST_FACTORS", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({
            totalAmount,
            term: 12,
            revenue,
            tenantCode: Tenant.TST_FACTORS,
            // @ts-expect-error Type 'string' is not assignable to type 'undefined'
            mccCode: "5912",
        })

        expect(rate).toEqual(new BigNumber("0.0117453605825698849"))
    })

    test.only("should calculate a dynamic rate with 'default' factor for TLC", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({
            totalAmount,
            term: 12,
            revenue,
            tenantCode: Tenant.TLC,
            // @ts-expect-error Type 'string' is not assignable to type 'undefined'
            mccCode: "9999",
        })

        expect(rate).toEqual(new BigNumber("0.0117453605825698849"))
    })

    test("should calculate a dynamic rate with given factor for TLC", () => {
        const revenue = money("3225000", Currency.EUR)
        const totalAmount = money("5000000", Currency.EUR)

        const rate = calculateRate({
            totalAmount,
            term: 12,
            revenue,
            tenantCode: Tenant.TLC,
            // @ts-expect-error Type 'string' is not assignable to type 'undefined'.ts
            mccCode: "5912",
        })

        expect(rate).toEqual(new BigNumber("0.03011630918607662794"))
    })
})

describe("calculatePayoutAmount", () => {
    test("should deduct the fee from the total amount", () => {
        const desiredAmount = money("1500000", Currency.EUR)

        const payoutAmount = calculatePayoutAmount(Tenant.TEST, desiredAmount, 12)

        const expectedPayoutAmount = money("1339285.71428571428571428571", Currency.EUR)

        expect(payoutAmount).toEqual(expectedPayoutAmount)
    })
})

describe("recalculateTotalAmount", () => {
    test("should recalculate the total amount", () => {
        const desiredAmount = money("1500000", Currency.EUR)

        const payoutAmount = moneyDown(calculatePayoutAmount(Tenant.TEST, desiredAmount, 12))
        const totalAmount = moneyUp(recalculateTotalAmount(Tenant.TEST, payoutAmount, 12))

        expect(desiredAmount).toEqual(totalAmount)
    })
})

describe("calculateTermsWithPayoutAmount", () => {
    test("for a given payout amount and revenue should calculate all the possible terms", () => {
        const revenue = money("15000000", Currency.EUR)
        const payoutAmount = money("1500000", Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.TEST,
            revenue: revenue,
            payoutAmount: payoutAmount,
        })

        expect(terms).toEqual([])
    })

    test("for a given payout amount and revenue should return no terms when is above the threshold", () => {
        const revenue = money("3000000", Currency.EUR)
        const desiredAmount = money("1390000", Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.TEST,
            revenue: revenue,
            payoutAmount: desiredAmount,
        })

        expect(terms).toEqual([])
    })
})

describe("calculateMaxOfferWithMinTerm", () => {
    test("should return the max amount with the min term", () => {
        const revenue = money("15000000", Currency.EUR)

        const offer = calculateMaxOfferWithMinTerm({ tenantCode: Tenant.TEST, revenue })

        const expected = {
            maxAmount: money("1500000", Currency.EUR),
            term: 6,
        }
        expect(offer).toEqual(expected)
    })

    test("should return some amount with the longest term", () => {
        const revenue = money("1500000", Currency.EUR)

        const offer = calculateMaxOfferWithMinTerm({ tenantCode: Tenant.TEST, revenue })

        const expected = {
            maxAmount: money("630000", Currency.EUR),
            term: 12,
        }
        expect(offer).toEqual(expected)
    })

    test("should return the highest amount with the longest term", () => {
        const revenue = money("3800000", Currency.EUR)

        const offer = calculateMaxOfferWithMinTerm({ tenantCode: Tenant.TEST, revenue })

        const expected = {
            maxAmount: money("1500000", Currency.EUR),
            term: 12,
        }
        expect(offer).toEqual(expected)
    })
})

describe("round money", () => {
    test("round 12398 to 12300", () => {
        const moneyToRound = money("12398", Currency.EUR)

        const moneyDown = roundMoneyDown(moneyToRound)

        const expected = money("12300", Currency.EUR)

        expect(moneyDown).toEqual(expected)
    })
    test("round 5797 to 5700", () => {
        const moneyToRound = money("5797", Currency.EUR)

        const moneyDown = roundMoneyDown(moneyToRound)

        const expected = money("5700", Currency.EUR)

        expect(moneyDown).toEqual(expected)
    })

    test("round 112 to 110", () => {
        const moneyToRound = money("112", Currency.EUR)

        const moneyDown = roundMoneyDown(moneyToRound)

        const expected = money("110", Currency.EUR)

        expect(moneyDown).toEqual(expected)
    })

    test("round down", () => {
        const moneyToRound = money("485436.89320388349514563107")

        expect(moneyDown(moneyToRound)).toEqual(money("485436"))
    })

    test("round up", () => {
        const moneyToRound = money("14563.10679611650485436893")

        expect(moneyUp(moneyToRound)).toEqual(money("14564"))
    })
})

describe("getFactor", () => {
    test("given a null factor it should return a default one", () => {
        const industry = null

        // @ts-ignore
        const factor = getFactor(Tenant.TEST, industry)

        expect(factor).toEqual(new BigNumber(0.035))
    })

    test("given no factor it should return a default one", () => {
        const factor = getFactor(Tenant.TEST)

        expect(factor).toEqual(new BigNumber(0.035))
    })

    test("given a factor it should return a proper value irrespective of the case", () => {
        const factor = getFactor(Tenant.BXY)

        expect(factor).toEqual(new BigNumber(0.2))
    })

    test("given an industry code it should get the right factor for payone", () => {
        expect(getFactor(Tenant.PAY, "GASTRONOMY" as IndustryCodes)).toEqual(new BigNumber(0.2))
        expect(getFactor(Tenant.PAY, "HOTEL_AND_LODGING" as IndustryCodes)).toEqual(
            new BigNumber(0.15),
        )
        expect(getFactor(Tenant.PAY, "HAIR_AND_BEAUTY_SALON" as IndustryCodes)).toEqual(
            new BigNumber(0.25),
        )
    })

    test("given a mcc code it should get the right factor for TLC", () => {
        expect(getFactor(Tenant.TLC, "5999" as IndustryCodes)).toEqual(new BigNumber(1.158))
        expect(getFactor(Tenant.TLC, "7230" as IndustryCodes)).toEqual(new BigNumber(1.25))
        expect(getFactor(Tenant.TLC, "4111" as IndustryCodes)).toEqual(new BigNumber(0.75))
    })
})

describe("getCCFactor", () => {
    test("given an unknown industry returns default for tlc", () => {
        const industry = "6666666666666666"
        const factor = getCCFactor(Tenant.TLC, industry as IndustryCodes)
        expect(factor).toEqual(new BigNumber(5))
    })

    test("given a mcc code it should get the right factor for TLC", () => {
        expect(getCCFactor(Tenant.TLC, "5999")).toEqual(new BigNumber(5.79))
        expect(getCCFactor(Tenant.TLC, "7230")).toEqual(new BigNumber(5.0))
        expect(getCCFactor(Tenant.TLC, "5942")).toEqual(new BigNumber(6.85))
    })
})
