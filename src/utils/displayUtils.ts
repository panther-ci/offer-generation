import {
    recalculateTotalAmount,
    calculateFee,
    calculateRate,
    calculateTermsWithPayoutAmount,
} from "../baseCalculations"
import { Term } from "../configuration"
import { LoanPurpose } from "./loanPurpose"
import { Money, moneyDtoToMoney, roundMoneyDown, MoneyDto } from "./money"
import { hasCcFactor, isDynamicRepayment, Tenant } from "./tenant"

export const convertEuroCentsToEuro = (cents: any) => cents / 100
export const convertEuroToEuroCents = (euro: any) => euro * 100

/** Converts Money to a number */
export const numberFromMoney = (money: Money) => Number(money.value)

/** Converts a number to Money format */
export const moneyFromNumber = (value: number, currency?: string) =>
    moneyDtoToMoney({
        value: String(value),
        currency: currency || "EUR",
    })

/** Rounds a calculated value in the required format as specified by the offer-gen library */
export const roundCalculatedValue = (value: number) => {
    const roundedValue = Math.round(value)
    const roundedValueInMoney = moneyFromNumber(roundedValue)
    const libRoundedMoney = roundMoneyDown(roundedValueInMoney)
    const libRoundedValue = Number(libRoundedMoney.value)
    return libRoundedValue
}

/** Performs a rounding calculation provided by the offer gen lib to calculate an amount */
export const calculateDisplayedRoundedAmount = (amount: MoneyDto) => {
    const roundedAmountInMoney = moneyDtoToMoney(amount)
    const roundedAmountInEuro = Number(convertEuroCentsToEuro(roundedAmountInMoney.value))

    return roundCalculatedValue(roundedAmountInEuro)
}

/** Calculates the loan offer fee
 * /**
 * @param {number} payoutAmount - the total loan offer payout amount in Euros.
 * @param {string} tenantCode - the Banxware tenant code.
 * @param {number} loanTerm - the selected term of the loan.
 * @param {string} [loanPurpose] - The purpose of the loan.
 */
export const calculateDisplayedOfferFee = (
    payoutAmount: number,
    tenantCode: Tenant,
    loanTerm: Term,
    loanPurpose?: LoanPurpose,
) => {
    // Adjusted offer based on the tenant's fee per month and selected term
    const adjustedTotalAmountInMoney = recalculateTotalAmount(
        tenantCode,
        moneyFromNumber(convertEuroToEuroCents(payoutAmount)),
        loanTerm,
        loanPurpose,
    )

    const feeInMoney = calculateFee(tenantCode, adjustedTotalAmountInMoney, loanTerm, loanPurpose)
    const feeRoundMoneyDown = roundMoneyDown(feeInMoney)
    const numb = numberFromMoney(feeRoundMoneyDown)
    const feeInEuro = convertEuroCentsToEuro(numb)

    return feeInEuro
}

/** Calculates the loan offer repayment rate
 * /**
 * @param {number} payoutAmount - the total loan offer payout amount in Euros.
 * @param {string} tenantCode - the Banxware tenant code.
 * @param {number} loanTerm - the selected term of the loan.
 * @param {number} revenue - the revenue of the applicant merchant in Euros.
 * @param {string} [mccCode] - an MCC industry code
 *  * @param {string} [loanPurpose] - The purpose of the loan.
 */
export const calculateDisplayedOfferRate = (
    payoutAmount: number,
    tenantCode: Tenant,
    loanTerm: Term,
    revenue: number,
    mccCode?: string,
    loanPurpose?: LoanPurpose,
) => {
    // otherwise `calculateRate` will throw
    if (hasCcFactor(tenantCode) && !mccCode) {
        return undefined
    }
    const revenueInMoney = moneyFromNumber(convertEuroToEuroCents(revenue))

    const adjustedTotalAmountInMoney = recalculateTotalAmount(
        tenantCode,
        moneyFromNumber(convertEuroToEuroCents(payoutAmount)),
        loanTerm,
        loanPurpose,
    )

    const rate = calculateRate({
        totalAmount: adjustedTotalAmountInMoney,
        term: loanTerm,
        revenue: revenueInMoney,
        tenantCode,
        // @ts-expect-error Type 'string' is not assignable to type 'never'.
        mccCode,
    })

    const dynamicOrFixedRate = isDynamicRepayment(tenantCode)
        ? rate.decimalPlaces(4).multipliedBy(100).toNumber()
        : convertEuroCentsToEuro(revenueInMoney.value.multipliedBy(rate).toNumber())

    return dynamicOrFixedRate
}

/** Calculates the possible loan terms for a selected payout amount
 * /**
 * @param {number} payoutAmount - the total loan offer payout amount in Euros.
 * @param {string} tenantCode - the Banxware tenant code.
 * @param {number} revenue - the revenue of the applicant merchant in Euros.
 * @param {string} [mccCode] - an MCC industry code.
 * @param {string} [loanPurpose] - The purpose of the loan.
 */
export const calculateDisplayedPossibleLoanTerms = (
    payoutAmount: number,
    tenantCode: Tenant,
    revenue: number,
    mccCode?: string,
    loanPurpose?: LoanPurpose,
) => {
    const revenueInMoney = moneyFromNumber(convertEuroToEuroCents(revenue))
    const payoutAmountInEuroCentsInMoney = moneyFromNumber(convertEuroToEuroCents(payoutAmount))

    const terms = calculateTermsWithPayoutAmount({
        tenantCode,
        revenue: revenueInMoney,
        payoutAmount: roundMoneyDown(payoutAmountInEuroCentsInMoney),
        // @ts-expect-error Type 'string' is not assignable to type 'never'.
        mccCode,
        loanPurpose,
    })

    return terms
}
