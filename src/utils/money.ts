// money
import BigNumber from "bignumber.js"

export enum Currency {
    EUR = "EUR",
}

export type Money = {
    value: BigNumber
    currency: Currency
}

export const money = (amount: string | BigNumber, currency?: Currency): Money => {
    const value = typeof amount === "string" ? new BigNumber(amount) : amount

    return {
        value,
        currency: currency || Currency.EUR,
    }
}

export type MoneyDto = {
    value: string
    currency: string
}

export const moneyToMoneyDto = (money: Money): MoneyDto => {
    return {
        value: money.value.toFixed(),
        currency: money.currency.toString(),
    }
}

export const moneyDtoToMoney = (moneyDto: MoneyDto): Money => {
    const currency = Currency[moneyDto.currency as keyof typeof Currency]

    return money(moneyDto.value, currency)
}

const roundDown = (toRoundDown: BigNumber, base: number): BigNumber =>
    toRoundDown.dividedToIntegerBy(new BigNumber(base)).multipliedBy(new BigNumber(base))

export const roundMoneyDown = (moneyToRound: Money): Money => {
    let roundedMoney = moneyToRound.value

    if (moneyToRound.value.isGreaterThanOrEqualTo(new BigNumber(100))) {
        roundedMoney = roundDown(moneyToRound.value, 10)
    }
    if (moneyToRound.value.isGreaterThanOrEqualTo(new BigNumber(1000))) {
        roundedMoney = roundDown(moneyToRound.value, 100)
    } else if (moneyToRound.value.isGreaterThanOrEqualTo(new BigNumber(10000))) {
        roundedMoney = roundDown(moneyToRound.value, 1000)
    }

    return {
        value: roundedMoney,
        currency: moneyToRound.currency,
    }
}

export const moneyDown = (money: Money): Money => {
    const value = money.value.decimalPlaces(0, BigNumber.ROUND_DOWN)

    return { ...money, value }
}

export const moneyUp = (money: Money): Money => {
    const value = money.value.decimalPlaces(0, BigNumber.ROUND_UP)

    return { ...money, value }
}
