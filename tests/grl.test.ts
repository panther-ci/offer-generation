import { Currency, money, Tenant, calculateMaxOfferWithMinTerm } from "../src"

describe("calculateMaxOfferWithMinTerm for GRL", () => {
    const { EUR } = Currency
    const { GRL } = Tenant
    const testCases = [
        {
            name: "returns maximum loan amount of 30k",
            testInput: { revenue: 100_000_00 },
            expected: { maxAmount: 30_000_00, term: 3 },
        },
        {
            name: "returns maximum lower than factor boundary",
            testInput: { revenue: 20_000_00 },
            expected: { maxAmount: 22_900_00, term: 12 },
        },
        {
            name: "returns maximum lower than factor boundary for low revenue",
            testInput: { revenue: 2_000_00 },
            expected: { maxAmount: 2_200_00, term: 12 },
        },
    ]

    test.each(testCases)("$name", ({ name, testInput, expected }) => {
        const offer = calculateMaxOfferWithMinTerm({
            tenantCode: GRL,
            revenue: money(String(testInput.revenue), EUR),
        })
        expect(offer).toEqual({ maxAmount: money(String(expected.maxAmount)), term: expected.term })
    })
})
