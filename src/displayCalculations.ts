import { LoanPurpose, recalculateTotalAmount } from "./baseCalculations"
import { Term } from "./configuration"
import {
    convertEuroCentsToEuro,
    calculateDisplayedRoundedAmount,
    calculateDisplayedOfferFee,
    calculateDisplayedOfferRate,
    calculateDisplayedPossibleLoanTerms,
    moneyFromNumber,
    convertEuroToEuroCents,
    numberFromMoney,
    roundCalculatedValue,
} from "./utils/displayUtils"
import { MoneyDto } from "./utils/money"
import { Tenant } from "./utils/tenant"

type OutputFormat = "rounded" | "raw"

export type CalculateDisplayOfferArgs = {
    /** Specify whether to return the values in a rounded or raw format */
    format?: OutputFormat
    /** Revenue in MoneyDto */
    revenue: MoneyDto
    /** max amount in MoneyDto */
    maxAmount: MoneyDto
    /** min amount in MoneyDto */
    minAmount: MoneyDto
    /** Tenant code */
    tenantCode: Tenant
    /** The users requested payout amount - a value that must lie in between the min and max of the offer - should be sent in whole currency, not cents  */
    requestedPayoutAmount: number
    /** The chosen term for the offer calculations to be made with */
    term: Term
    /** The chosen purpose for the offer calculations to be made with */
    loanPurpose: LoanPurpose
    /** The mcc/industry code to be passed and factored into the offer */
    mccCode?: string
}

export type CalculateDisplayOfferReturn = {
    /** The current selected payout amount in euros */
    payoutAmount: number
    /** The monthly repayment rate of the loan */
    rate: number
    /** The possible loan terms based on the desired payout */
    possibleLoanTerms: Array<number>
    /** The loan fee in euros */
    feeInEuro: number
    /** The final total loan amount after all adjustments have been made */
    finalTotalLoanAmount: number
}

export type CalculateDisplayOfferSignature = (
    args: CalculateDisplayOfferArgs,
) => CalculateDisplayOfferReturn

const formattedValue = (value: number, format: OutputFormat) =>
    format === "rounded" ? roundCalculatedValue(value) : value

export const calculateDisplayOffer: CalculateDisplayOfferSignature = ({
    term,
    maxAmount,
    minAmount,
    requestedPayoutAmount,
    revenue,
    tenantCode,
    loanPurpose,
    mccCode,
    format = "rounded",
}) => {
    const revenueValue = convertEuroCentsToEuro(Number(revenue.value))

    const minPayoutAmount = calculateDisplayedRoundedAmount(minAmount)
    const maxPayoutAmount = calculateDisplayedRoundedAmount(maxAmount)

    const maxMonthlyPayoutAmountInEuro = (maxPayoutAmount as unknown as number) / term

    const maxMonthlyLoanAmountMultipliedByTermInEuro = maxMonthlyPayoutAmountInEuro * term
    // Recalculate the loan amount when the selected term changes
    const recalculatedPayoutAmount =
        maxMonthlyLoanAmountMultipliedByTermInEuro < maxPayoutAmount
            ? maxMonthlyLoanAmountMultipliedByTermInEuro
            : maxPayoutAmount

    const payoutAmount = formattedValue(
        recalculatedPayoutAmount < requestedPayoutAmount
            ? recalculatedPayoutAmount
            : requestedPayoutAmount,
        format,
    )

    const finalTotalLoanAmount = recalculateTotalAmount(
        tenantCode,
        moneyFromNumber(convertEuroToEuroCents(payoutAmount)),
        term,
        loanPurpose,
    )

    const displayFinalTotalLoanAmount = formattedValue(
        convertEuroCentsToEuro(numberFromMoney(finalTotalLoanAmount)),
        format,
    )

    const feeInEuro = formattedValue(
        calculateDisplayedOfferFee(payoutAmount, tenantCode, term, loanPurpose),
        format,
    )

    const rate = calculateDisplayedOfferRate(
        payoutAmount,
        tenantCode,
        term,
        revenueValue,
        mccCode,
        loanPurpose,
    )!

    const possibleLoanTerms = calculateDisplayedPossibleLoanTerms(
        payoutAmount,
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        tenantCode,
        revenueValue,
        mccCode,
        loanPurpose,
    )

    const values = {
        payoutAmount,
        feeInEuro,
        maxPayoutAmount,
        minPayoutAmount,
        rate,
        possibleLoanTerms,
        finalTotalLoanAmount: displayFinalTotalLoanAmount,
    }

    return values
}
