// tenant configuration
import BigNumber from "bignumber.js"

import { Currency, money, Money } from "./utils/money"
import { Tenant } from "./utils/tenant"
import { IndustryCodes, payFactors, tlcMccFactors } from "./factors"
import { LoanPurpose } from "./utils/loanPurpose"

export type Term = 3 | 6 | 9 | 12

export type PossibleTerms = Term[]

export type TenantConfig = {
    tenantKey: Tenant
    maxLoanAmount: Money
    minLoanAmount: Money
    minRevenueAmount?: Money
    defaultFactor: BigNumber
    factors?: (code: IndustryCodes) => BigNumber
    creditCardFactors?: { [key: string]: BigNumber }
    maxTermInMonths: number
    minTermInMonths: number
    possibleTerms: PossibleTerms
    minTimeOfRunBusinessInMonths: number
    feePerMonth: any | BigNumber
    eligibleCountries?: string[]
    loanPurpose?: LoanPurpose
}

const { Inventory, Marketing } = LoanPurpose

export const configuration: { [key in Tenant]: TenantConfig } = {
    BXY: {
        tenantKey: Tenant.BXY,
        maxLoanAmount: money("10000000", Currency.EUR),
        minLoanAmount: money("100000", Currency.EUR),
        minRevenueAmount: money("125000", Currency.EUR),
        defaultFactor: new BigNumber("0.15"),
        maxTermInMonths: 12,
        minTermInMonths: 3,
        possibleTerms: [3, 6, 12],
        feePerMonth: {
            [Marketing]: {
                [3]: new BigNumber("0.035"),
                [6]: new BigNumber("0.045"),
                [12]: new BigNumber("0.07"),
            },
            [Inventory]: {
                [3]: new BigNumber("0.036"),
                [6]: new BigNumber("0.078"),
                [12]: new BigNumber("0.168"),
            },
        },
        minTimeOfRunBusinessInMonths: 6,
        eligibleCountries: ["DEU"],
    },
    GRL: {
        tenantKey: Tenant.GRL,
        maxLoanAmount: money("3000000", Currency.EUR),
        minLoanAmount: money("200000", Currency.EUR),
        defaultFactor: new BigNumber("0.11"),
        maxTermInMonths: 12,
        minTermInMonths: 3,
        possibleTerms: [3, 6, 12],
        minTimeOfRunBusinessInMonths: 6,
        feePerMonth: new BigNumber("0.0125"),
    },
    BXW: {
        tenantKey: Tenant.BXW,
        maxLoanAmount: money("10000000", Currency.EUR),
        minLoanAmount: money("45000", Currency.EUR),
        minRevenueAmount: money("40000", Currency.EUR),
        defaultFactor: new BigNumber("0.12"),
        maxTermInMonths: 12,
        minTermInMonths: 3,
        possibleTerms: [3, 6, 12],
        minTimeOfRunBusinessInMonths: 3,
        feePerMonth: {
            [3]: new BigNumber("0.045"),
            [6]: new BigNumber("0.096"),
            [12]: new BigNumber("0.204"),
        },
    },
    TEST: {
        tenantKey: Tenant.TEST,
        maxLoanAmount: money("1500000", Currency.EUR),
        minLoanAmount: money("300000", Currency.EUR),
        defaultFactor: new BigNumber("0.035"),
        maxTermInMonths: 12,
        minTermInMonths: 6,
        possibleTerms: [6, 9, 12],
        minTimeOfRunBusinessInMonths: 3,
        feePerMonth: new BigNumber("0.01"),
    },
    TST_FACTORS: {
        tenantKey: Tenant.TST_FACTORS,
        maxLoanAmount: money("1500000", Currency.EUR),
        minLoanAmount: money("300000", Currency.EUR),
        defaultFactor: new BigNumber("0.15").multipliedBy("5"),
        factors: tlcMccFactors,
        maxTermInMonths: 12,
        minTermInMonths: 6,
        possibleTerms: [6, 9, 12],
        minTimeOfRunBusinessInMonths: 3,
        feePerMonth: new BigNumber("0.01"),
    },
    PAY: {
        tenantKey: Tenant.PAY,
        maxLoanAmount: money("5000000", Currency.EUR),
        minLoanAmount: money("100000", Currency.EUR),
        minRevenueAmount: money("100000", Currency.EUR),
        defaultFactor: new BigNumber("0.15"),
        factors: payFactors,
        maxTermInMonths: 12,
        minTermInMonths: 3,
        possibleTerms: [3, 6, 12],
        minTimeOfRunBusinessInMonths: 6,
        feePerMonth: new BigNumber("0.013"),
        eligibleCountries: ["DEU"],
    },
    TLC: {
        tenantKey: Tenant.TLC,
        maxLoanAmount: money("5000000", Currency.EUR),
        minLoanAmount: money("100000", Currency.EUR),
        minRevenueAmount: money("50000", Currency.EUR),
        defaultFactor: new BigNumber("0.15").multipliedBy("5"),
        factors: tlcMccFactors,
        maxTermInMonths: 12,
        minTermInMonths: 3,
        possibleTerms: [3, 6, 12],
        minTimeOfRunBusinessInMonths: 3,
        feePerMonth: new BigNumber("0.013"),
        eligibleCountries: ["DEU"],
    },
}
