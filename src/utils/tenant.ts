export enum Tenant {
    BXY = "BXY",
    BXW = "BXW",
    GRL = "GRL",
    PAY = "PAY",
    TEST = "TEST",
    TST_FACTORS = "TST_FACTORS",
    TLC = "TLC",
}

export const isBanxy = (tenant: Tenant) => Tenant.BXY === tenant
export const isTelecash = (tenant: Tenant) => Tenant.TLC === tenant
export const isTstFactors = (tenant: Tenant) => Tenant.TST_FACTORS === tenant
export const isDynamicRepayment = (tenant: Tenant) => isBanxy(tenant) || isTelecash(tenant)
export const hasCcFactor = (tenant: Tenant) => isTelecash(tenant) || isTstFactors(tenant)
