import BigNumber from "bignumber.js"
import { Term, LoanPurpose } from "../src"
import {
    calculateDisplayedOfferFee,
    calculateDisplayedOfferRate,
    moneyFromNumber,
    numberFromMoney,
} from "../src/utils/displayUtils"

describe("utils/displayUtils -> moneyFromNumber", () => {
    it("converts a numerical value to a Money value", () => {
        expect(moneyFromNumber(10000)).toMatchObject({
            currency: "EUR",
            value: new BigNumber(10000),
        })
    })
})

describe("utils/displayUtils -> numberFromMoney", () => {
    it("converts a numerical value to a Money value", () => {
        expect(numberFromMoney({ value: new BigNumber(10000), currency: "EUR" as any })).toEqual(
            10000,
        )
    })
})

describe("utils/displayUtils -> calculateDisplayedOfferFee", () => {
    const offerFeeTestValues: Array<{
        payoutAmount: number
        term: number
        expectedFee: number
        tenant: any
        loanPurpose: LoanPurpose
    }> = [
        {
            tenant: "BXY",
            expectedFee: 645,
            payoutAmount: 14355,
            term: 6,
            loanPurpose: LoanPurpose.Marketing,
        },
        {
            tenant: "BXY",
            expectedFee: 981,
            payoutAmount: 14019,
            term: 12,
            loanPurpose: LoanPurpose.Marketing,
        },
        {
            tenant: "BXY",
            expectedFee: 1103,
            payoutAmount: 14151,
            term: 6,
            loanPurpose: LoanPurpose.Inventory,
        },
        {
            tenant: "BXY",
            expectedFee: 2322,
            payoutAmount: 13825,
            term: 12,
            loanPurpose: LoanPurpose.Inventory,
        },
    ]

    it("Returns the correct loan fee amount based on a set of pre validated values", () => {
        offerFeeTestValues.forEach((v) => {
            expect(
                calculateDisplayedOfferFee(v.payoutAmount, v.tenant, v.term as Term, v.loanPurpose),
            ).toEqual(v.expectedFee)
        })
    })
})

describe("utils/displayUtils -> calculateDisplayedOfferRate", () => {
    const revenue = 50000
    const rateTestValues: Array<{
        payoutAmount: number
        term: number
        expectedRate: number
        tenant: any
        revenue: number
        loanPurpose: LoanPurpose
    }> = [
        {
            tenant: "BXY",
            expectedRate: 6.69,
            payoutAmount: 37500,
            term: 12,
            revenue,
            loanPurpose: LoanPurpose.Marketing,
        },
        {
            tenant: "BXY",
            expectedRate: 1.78,
            payoutAmount: 10000,
            term: 12,
            revenue,
            loanPurpose: LoanPurpose.Marketing,
        },
        {
            tenant: "BXY",
            expectedRate: 1.95,
            payoutAmount: 10000,
            term: 12,
            revenue,
            loanPurpose: LoanPurpose.Inventory,
        },
        {
            tenant: "BXY",
            expectedRate: 0.54,
            payoutAmount: 3000,
            term: 12,
            revenue,
            loanPurpose: LoanPurpose.Marketing,
        },
        {
            tenant: "BXY",
            expectedRate: 6.97,
            payoutAmount: 20000,
            term: 6,
            revenue,
            loanPurpose: LoanPurpose.Marketing,
        },
    ]

    it("Returns the correct loan fee amount based on a set of pre validated values", () => {
        rateTestValues.forEach((v) => {
            expect(
                calculateDisplayedOfferRate(
                    v.payoutAmount,
                    v.tenant,
                    v.term as Term,
                    v.revenue,
                    undefined,
                    v.loanPurpose,
                ),
            ).toEqual(v.expectedRate)
        })
    })
})
