import {
    calculateDisplayedOfferFee,
    calculateDisplayedOfferRate,
    calculateDisplayedPossibleLoanTerms,
    calculateDisplayOffer,
    configuration,
    convertEuroCentsToEuro,
    Currency,
    LoanPurpose,
    money,
    moneyToMoneyDto,
    numberFromMoney,
    Tenant,
    Term,
} from "../src"

const testValues = [
    {
        tenant: Tenant.BXY,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.BXY.maxLoanAmount,
        minOffer: configuration.BXY.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        loanPurpose: LoanPurpose.Marketing,
        term: 12 as Term,
    },
    {
        tenant: Tenant.BXY,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.BXY.maxLoanAmount,
        minOffer: configuration.BXY.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        loanPurpose: LoanPurpose.Marketing,
        term: 6 as Term,
    },
    {
        tenant: Tenant.GRL,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.GRL.maxLoanAmount,
        minOffer: configuration.GRL.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        term: 6 as Term,
    },
    {
        tenant: Tenant.GRL,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.GRL.maxLoanAmount,
        minOffer: configuration.GRL.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        term: 12 as Term,
    },
    {
        tenant: Tenant.PAY,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.PAY.maxLoanAmount,
        minOffer: configuration.PAY.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        term: 12 as Term,
        mcc: "GASTRONOMY",
    },
    {
        tenant: Tenant.TLC,
        revenue: money("1000000", Currency.EUR),
        maxOffer: configuration.PAY.maxLoanAmount,
        minOffer: configuration.PAY.minLoanAmount,
        requestedInMoney: money("1500000", Currency.EUR),
        requestedNumeric: convertEuroCentsToEuro(numberFromMoney(money("1500000", Currency.EUR))),
        term: 12 as Term,
        mcc: "5172",
    },
]

describe("src/displayCalculations -> calculateDisplayOffer", () => {
    testValues.forEach((testValue) => {
        it(`For Tenant: ${testValue.tenant} calculates an expected Display Offer`, () => {
            const val = calculateDisplayOffer({
                revenue: moneyToMoneyDto(testValue.revenue),
                maxAmount: moneyToMoneyDto(testValue.maxOffer),
                minAmount: moneyToMoneyDto(testValue.minOffer),
                term: testValue.term,
                tenantCode: testValue.tenant,
                requestedPayoutAmount: testValue.requestedNumeric,
                format: "raw",
                loanPurpose: testValue.loanPurpose!,
                mccCode: testValue.mcc,
            })

            const expectedFee = calculateDisplayedOfferFee(
                testValue.requestedNumeric,
                testValue.tenant,
                testValue.term,
                testValue.loanPurpose,
            )

            const expectedTotal = testValue.requestedNumeric + expectedFee

            const expectedRate = calculateDisplayedOfferRate(
                testValue.requestedNumeric,
                testValue.tenant,
                testValue.term,
                convertEuroCentsToEuro(numberFromMoney(testValue.revenue)),
                testValue.mcc,
                testValue.loanPurpose,
            )

            const expectedTerms = calculateDisplayedPossibleLoanTerms(
                testValue.requestedNumeric,
                testValue.tenant,
                convertEuroCentsToEuro(numberFromMoney(testValue.revenue)),
                testValue.mcc,
                testValue.loanPurpose,
            )

            expect(val).toMatchObject({
                feeInEuro: expectedFee,
                finalTotalLoanAmount: expectedTotal,
                rate: expectedRate,
                payoutAmount: testValue.requestedNumeric,
                possibleLoanTerms: expectedTerms,
                maxPayoutAmount: convertEuroCentsToEuro(numberFromMoney(testValue.maxOffer)),
                minPayoutAmount: convertEuroCentsToEuro(numberFromMoney(testValue.minOffer)),
            })
        })
    })
})
