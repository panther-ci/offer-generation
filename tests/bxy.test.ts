import {
    calculateFee,
    calculateMaxOfferWithMinTerm,
    calculatePayoutAmount,
    calculateTermsWithPayoutAmount,
    getFactor,
    LoanPurpose,
    recalculateTotalAmount,
    Currency,
    money,
    moneyDown,
    moneyUp,
    Tenant,
} from "../src"
import BigNumber from "bignumber.js"

describe("calculateFee for banxy", () => {
    test("should calculate fee for banxy with marketing purpose and 6 months tenure", () => {
        const totalAmount = money("1500000", Currency.EUR)

        let fee = calculateFee(Tenant.BXY, totalAmount, 6, LoanPurpose.Marketing)
        let expectedFee = money("64593.3014354066985645933", Currency.EUR)
        expect(fee).toEqual(expectedFee)
    })

    test("should calculate fee for banxy with marketing purpose and 12 months tenure", () => {
        const totalAmount = money("1500000", Currency.EUR)

        let fee = calculateFee(Tenant.BXY, totalAmount, 12, LoanPurpose.Marketing)
        let expectedFee = money("98130.84112149532710280374", Currency.EUR)
        expect(fee).toEqual(expectedFee)
    })

    test("should calculate fee for banxy with inventory purpose and 6 months tenure", () => {
        const totalAmount = money("1500000", Currency.EUR)

        let fee = calculateFee(Tenant.BXY, totalAmount, 6, LoanPurpose.Inventory)
        let expectedFee = money("108534.32282003710575139147", Currency.EUR)
        expect(fee).toEqual(expectedFee)
    })

    test("should calculate fee for banxy with inventory purpose and 12 months tenure", () => {
        const totalAmount = money("1500000", Currency.EUR)

        let fee = calculateFee(Tenant.BXY, totalAmount, 12, LoanPurpose.Inventory)
        let expectedFee = money("215753.42465753424657534247", Currency.EUR)
        expect(fee).toEqual(expectedFee)
    })
})

describe("calculatePayoutAmount for banxy", () => {
    test("calculatePayoutAmount amount for 6 months and marketing purpose", () => {
        const totalAmount = money("1500000", Currency.EUR)

        const payoutAmount = calculatePayoutAmount(
            Tenant.BXY,
            totalAmount,
            6,
            LoanPurpose.Marketing,
        )

        const expectedPayoutAmount = money("1435406.6985645933014354067", Currency.EUR)

        expect(payoutAmount).toEqual(expectedPayoutAmount)
    })

    test("calculatePayoutAmount amount for 12 months and marketing purpose", () => {
        const totalAmount = money("1500000", Currency.EUR)

        const payoutAmount = calculatePayoutAmount(
            Tenant.BXY,
            totalAmount,
            12,
            LoanPurpose.Marketing,
        )

        const expectedPayoutAmount = money("1401869.15887850467289719626", Currency.EUR)

        expect(payoutAmount).toEqual(expectedPayoutAmount)
    })

    test("calculatePayoutAmount amount for 6 months and inventory purpose", () => {
        const totalAmount = money("1500000", Currency.EUR)

        const payoutAmount = calculatePayoutAmount(
            Tenant.BXY,
            totalAmount,
            6,
            LoanPurpose.Inventory,
        )

        const expectedPayoutAmount = money("1391465.67717996289424860853", Currency.EUR)

        expect(payoutAmount).toEqual(expectedPayoutAmount)
    })

    test("calculatePayoutAmount amount for 12 months and inventory purpose", () => {
        const totalAmount = money("1500000", Currency.EUR)

        const payoutAmount = calculatePayoutAmount(
            Tenant.BXY,
            totalAmount,
            12,
            LoanPurpose.Inventory,
        )

        const expectedPayoutAmount = money("1284246.57534246575342465753", Currency.EUR)

        expect(payoutAmount).toEqual(expectedPayoutAmount)
    })
})

describe("recalculateTotalAmount for banxy ", () => {
    test("should recalculate the total amount", () => {
        const totalAmount = money("1500000", Currency.EUR)

        const payoutAmount = moneyDown(
            calculatePayoutAmount(Tenant.BXY, totalAmount, 12, LoanPurpose.Marketing),
        )
        const calculatedTotalAmount = moneyUp(
            recalculateTotalAmount(Tenant.BXY, payoutAmount, 12, LoanPurpose.Marketing),
        )

        expect(calculatedTotalAmount).toEqual(totalAmount)
    })
})

describe("calculateTermsWithPayoutAmount for banxy", () => {
    const testCases = [
        {
            revenue: 40_000_00,
            desiredAmount: 96_000_00,
            loanPurpose: LoanPurpose.Marketing,
            expected: [],
        },
        {
            revenue: 40_000_00,
            desiredAmount: 89_700_00,
            loanPurpose: LoanPurpose.Marketing,
            expected: [],
        },
        {
            revenue: 40_000_00,
            desiredAmount: 46_000_00,
            loanPurpose: LoanPurpose.Marketing,
            expected: [12],
        },
        {
            revenue: 15000000,
            desiredAmount: 1500000,
            loanPurpose: LoanPurpose.Marketing,
            expected: [3, 6, 12],
        },
        {
            revenue: 20_000_00,
            desiredAmount: 48_001_00,
            loanPurpose: LoanPurpose.Marketing,
            expected: [],
        },
        {
            revenue: 20_000_00,
            desiredAmount: 24_000_00,
            loanPurpose: LoanPurpose.Marketing,
            expected: [12],
        },
    ]
    test.each(testCases)(
        "for payout of $desiredAmount and revenue of $revenue correct terms are returned",
        ({ revenue, desiredAmount, loanPurpose, expected }) => {
            const terms = calculateTermsWithPayoutAmount({
                tenantCode: Tenant.BXY,
                revenue: money(String(revenue), Currency.EUR),
                payoutAmount: money(String(desiredAmount), Currency.EUR),
                loanPurpose,
            })
            expect(terms).toEqual(expected)
        },
    )

    test("for payout exact payout max that's over 20% rate it returns empty array", () => {
        const revenue = money(String(20_000_00), Currency.EUR)
        const payoutAmount = money(String(48_000_00), Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.BXY,
            revenue: revenue,
            payoutAmount: payoutAmount,
            loanPurpose: LoanPurpose.Marketing,
        })
        expect(terms).toEqual([])
    })

    test("for a given exactly maximum payout amount and revenue should calculate all the possible terms", () => {
        const revenue = money("15000000", Currency.EUR)
        const payoutAmount = money(String(100_000_00), Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.BXY,
            revenue: revenue,
            payoutAmount: payoutAmount,
            loanPurpose: LoanPurpose.Marketing,
        })

        expect(terms).toEqual([6, 12])
    })

    test("for a given payout amount and revenue should calculate all the possible terms for Inventory", () => {
        const revenue = money("15000000", Currency.EUR)
        const payoutAmount = money("1500000", Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.BXY,
            revenue: revenue,
            payoutAmount: payoutAmount,
            loanPurpose: LoanPurpose.Marketing,
        })

        expect(terms).toEqual([3, 6, 12])
    })

    test("for a given payout amount and revenue should return no terms when is above the threshold", () => {
        const revenue = money("3000000", Currency.EUR)
        const desiredAmount = money("11000000", Currency.EUR)

        const terms = calculateTermsWithPayoutAmount({
            tenantCode: Tenant.BXY,
            revenue: revenue,
            payoutAmount: desiredAmount,
            loanPurpose: LoanPurpose.Marketing,
        })

        expect(terms).toEqual([])
    })
})

describe("calculateMaxOfferWithMinTerm for Banxy", () => {
    const { Marketing, Inventory } = LoanPurpose
    const testCases = [
        {
            name: "should return the max amount with the min term",
            testInput: { revenue: 150_000_00, loanPurpose: Inventory },
            expected: { maxAmount: 100_000_00, term: 6 },
        },
        {
            name: "should return the highest amount with the longest term",
            testInput: { revenue: 15_000_00, loanPurpose: Inventory },
            expected: { maxAmount: 23_100_00, term: 12 },
        },
        {
            name: "should return the highest amount with the longest term",
            testInput: { revenue: 38_000_00, loanPurpose: Inventory },
            expected: { maxAmount: 58_500_00, term: 12 },
        },
        {
            name: "should return max amount that doesn't go over KDF factor boundary, with marketing purpose",
            testInput: { revenue: 20_000_00, loanPurpose: Marketing },
            expected: { maxAmount: 33_600_00, term: 12 },
        },
        {
            name: "should return max amount that doesn't go over KDF factor boundary, with Invetory purpose",
            testInput: { revenue: 1_300_00, loanPurpose: Marketing },
            expected: { maxAmount: 2_100_00, term: 12 },
        },
        {
            name: "",
            testInput: { revenue: 30_000_00, loanPurpose: Marketing },
            expected: { maxAmount: 50_400_00, term: 12 },
        },
    ]

    const { EUR } = Currency
    const { BXY } = Tenant

    test.each(testCases)("$name", ({ name, testInput, expected }) => {
        const offer = calculateMaxOfferWithMinTerm({
            tenantCode: BXY,
            revenue: money(String(testInput.revenue), EUR),
            loanPurpose: testInput.loanPurpose,
        })
        expect(offer).toEqual({ maxAmount: money(String(expected.maxAmount)), term: expected.term })
    })
})

describe("getFactor for banxy", () => {
    test("given a null factor it should return a default one", () => {
        const industry = null

        // @ts-ignore
        const factor = getFactor(Tenant.BXY, industry)

        expect(factor).toEqual(new BigNumber(0.15))
    })

    test("given no factor it should return a default one", () => {
        const factor = getFactor(Tenant.BXY)

        expect(factor).toEqual(new BigNumber(0.15))
    })

    test("given a factor it should return a proper value irrespective of the case", () => {
        const factor = getFactor(Tenant.BXY)

        expect(factor).toEqual(new BigNumber(0.15))
    })
})
