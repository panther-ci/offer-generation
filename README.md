# Offer Generation

TL;DR

Simple lib to be cross used in another services/projects mitigating the code duplication.

## Developing

Install local dependencies using:

```shell
npm install
```

For validating changes locally, you can use the [yarn link](https://classic.yarnpkg.com/lang/en/docs/cli/link/) feature which will allow you to install your changes in another local repo. This is setup on the `start` command and can be achieved as follows:

In this repo, run:

```
npm run start
```

This will watch for changes and setup a link.

In the repo you want to test, run:

```
yarn link @banxware/offer-generation
```

You should now be able to make changes and see them where they are consumed.

Happy coding.

## Testing

```shell
npm test
```

## Using as a lib

```shell
npm install @banxware/offer-generation
```

All the public functions should be exposed in the index.ts file.

## New Releases

New versions of the library are automatically tagged and released by the CI pipeline on merge to the `main` branch.

## Misc

Found a bug? Open a PR.

Have written a different function? Write a test.
