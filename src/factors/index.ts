import BigNumber from "bignumber.js"
import { PayFactors, PAY_FACTORS } from "./data/payFactors"
import { TlcMccCodes, TLC_FACTORS } from "./data/tlcFactors"

export type IndustryCodes = TlcMccCodes & PayFactors

export const tlcMccFactors = (code: TlcMccCodes): BigNumber => {
    const loanFactor = new BigNumber(TLC_FACTORS[code].loanFactor)
    const CCFactor = new BigNumber(TLC_FACTORS[code].CCFactor)

    return loanFactor.multipliedBy(CCFactor)
}

export const payFactors = (code: PayFactors): BigNumber => new BigNumber(PAY_FACTORS[code])
